import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardMesinfoComponent } from './board-mesinfo.component';

describe('BoardmesinfoComponent', () => {
  let component: BoardMesinfoComponent;
  let fixture: ComponentFixture<BoardMesinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardMesinfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardMesinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
