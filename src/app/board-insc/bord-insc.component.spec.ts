import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardInscComponent } from './board-insc.component';

describe('BoardInscComponent', () => {
  let component: BoardInscComponent;
  let fixture: ComponentFixture<BoardInscComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardInscComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardInscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
