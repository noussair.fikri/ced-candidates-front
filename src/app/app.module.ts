import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardDtheseComponent } from './board-dthese/board-dthese.component';
import { BoardDoctorantComponent } from './board-doctorant/board-doctorant.component';
import { BoardCompteComponent } from './board-compte/board-compte.component';
import { BoardModifComponent } from './board-modif/board-modif.component';
import { BoardInscComponent } from './board-insc/board-insc.component';
import { BoardPreinscComponent } from './board-preinsc/board-preisnc.component';
import { BoardReinscComponent } from './board-reinsc/board-reinsc.component';
import { BoardDocuComponent } from './board-docu/board-docu.component';
import { BoardScoComponent } from './board-sco/board-sco.component';
import { BoardDeporecComponent } from './board-deporec/board-deporec.component';
import { BoardSuivrecComponent } from './board-suivrec/board-suivrec.component';
import { BoardReclComponent } from './board-recl/board-recl.component';
import { BoardMesdocComponent } from './board-mesdoc/board-mesdoc.component';
import { BoardInfogesdoComponent } from './board-infogesdo/board-infogesdo.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { AuthService } from './_services/auth.service';
import { BoardDmdsouComponent } from './board-dmdsou/board-dmdsou.component';
import { BoardCredmComponent } from './board-credm/board-credm.component';
import { BoardSuivsoComponent } from './board-suivso/board-suivso.component';
import { BoardActsciComponent } from './board-actsci/board-actsci.component';
import { BoardMesinfoComponent } from './board-mesinfo/board-mesinfo.component';
import { BoardGessouComponent } from './board-gessou/board-gessou.component';
import { BoardExrapComponent } from './board-exrap/board-exrap.component';
import { NgbdSortableHeader } from './_directives/sortable.directive';
import { BoardVicedoyComponent } from './board-vicedoy/board-vicedoy.component';
import { BoardDlaboComponent } from './board-dlabo/board-dlabo.component';
import { BoardInscritComponent } from './board-inscrit/board-inscrit.component';
import { BoardInfraLaboComponent } from './board-infralabo/board-infralabo.component';
import { BoardActiDocComponent } from './board-actidoc/board-actidoc.component';
import { BoardExaRapComponent } from './board-exarap/board-exarap.component';
import { BoardSoutComponent } from './board-sout/board-sout.component';
import { BoardDeroComponent } from './board-Derogation/board-dero.component';
import { BoardExaRapExaminateurComponent } from './board-exarap-examinateur/board-exarap-examinateur.component';





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent, 
    BoardDtheseComponent,
    BoardDoctorantComponent,
    BoardInfogesdoComponent,
    BoardCompteComponent,
    BoardModifComponent,
    BoardInscComponent,
    BoardPreinscComponent,
    BoardReinscComponent,
    BoardDocuComponent,
    BoardScoComponent,
    BoardReclComponent,
    BoardMesdocComponent,
    BoardDeporecComponent,
    BoardSuivrecComponent,
    BoardDmdsouComponent,
    BoardCredmComponent,
    BoardSuivsoComponent,
    BoardActsciComponent,
    BoardMesinfoComponent,
    BoardGessouComponent,
    BoardExrapComponent,
    NgbdSortableHeader,
    
    
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    

  ],
  providers: [authInterceptorProviders, AuthService],
  exports: [BoardAdminComponent,BoardDtheseComponent,BoardDoctorantComponent,BoardInfogesdoComponent,BoardModifComponent,],
  

  bootstrap: [AppComponent]
})
export class AppModule { }
