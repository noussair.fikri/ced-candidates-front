import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardReclComponent } from './board-recl.component';

describe('BoardReclComponent', () => {
  let component: BoardReclComponent;
  let fixture: ComponentFixture<BoardReclComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardReclComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardReclComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
