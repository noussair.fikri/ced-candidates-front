import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardMesdocComponent } from './board-mesdoc.component';

describe('BoardMesdocComponent', () => {
  let component: BoardMesdocComponent;
  let fixture: ComponentFixture<BoardMesdocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardMesdocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardMesdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
