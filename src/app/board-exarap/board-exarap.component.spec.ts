import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardExaRapComponent } from './board-exarap.component';

describe('BoardExaRapComponent', () => {
  let component: BoardExaRapComponent;
  let fixture: ComponentFixture<BoardExaRapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardExaRapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardExaRapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
