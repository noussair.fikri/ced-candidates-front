import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardActiDocComponent } from './board-actidoc.component';

describe('BoardActiDocComponent', () => {
  let component: BoardActiDocComponent;
  let fixture: ComponentFixture<BoardActiDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardActiDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardActiDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
