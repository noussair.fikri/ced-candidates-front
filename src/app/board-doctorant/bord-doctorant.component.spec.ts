import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDoctorantComponent } from './board-doctorant.component';

describe('BoardDoctorantComponent', () => {
  let component: BoardDoctorantComponent;
  let fixture: ComponentFixture<BoardDoctorantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDoctorantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDoctorantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
