import { Component, OnInit, PipeTransform, QueryList, ViewChildren } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { UserService } from '../_services/user.service';
import { DecimalPipe } from '@angular/common';
import { NgbdSortableHeader, SortEvent } from '../_directives/sortable.directive';
import { User } from '../_models/user';
import { Observable } from 'rxjs';
import { UserTableService } from '../_services/user.table.service';
import { FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css'],
  providers: [UserTableService, DecimalPipe]
})



export class BoardAdminComponent implements OnInit {
  content?: string;
  isLoggedIn = false;

  users$!: Observable<User[]>;
  total$!: Observable<number>;

  displayStyle = "none";

  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
 
  constructor(public userTableService: UserTableService, private userService: UserService, private tokenStorageService: TokenStorageService, private formBuilder: FormBuilder) { 
    this.users$ = userService.getUsers();
    this.total$ = userTableService.total$;
  }

  userForm = this.formBuilder.group({
    username: '',
    nom: '',
    prenom: '',
    cin: '',
    cne: '',
    email: '' 
  });

  userEditForm = this.formBuilder.group({
    id: 0,
    username: '',
    nom: '',
    prenom: '',
    cin: '',
    cne: '',
    email: '' 
  });

  elementToRemove = 0;

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.userTableService.sortColumn = column;
    this.userTableService.sortDirection = direction;
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

  onSubmit(): void {

    console.log(this.userForm.value);
    this.userService.addUser(this.userForm.value).subscribe(
      (response: User) => {
        console.log(response);
        this.users$ = this.userService.getUsers();
        this.userForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        //addForm.reset();
      }
    );

  }

  onEdit(): void {
    this.userService.updUser(this.userEditForm.value).subscribe(
      (response: User) => {
        console.log(response);
        this.users$ = this.userService.getUsers();
        this.userEditForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        //addForm.reset();
      }
    );

  }

  generateUsername(): void {
    if(this.userForm.value['nom'] != null && this.userForm.value['prenom'] != null)
      this.userForm.controls['username'].setValue(this.userForm.value['prenom']+'.'+this.userForm.value['nom']);
    if(this.userEditForm.value['nom'] != null && this.userEditForm.value['prenom'] != null)
      this.userEditForm.controls['username'].setValue(this.userEditForm.value['prenom']+'.'+this.userEditForm.value['nom']);
  }

  openEdit(id: any): void { 
    console.log(id);
    //this.displayStyle = "none";
    this.userService.getUser(id).subscribe(
      (response: User) => {
        console.log(response);
        //this.userEditForm!.get('username')!.setValue(response.username);
        this.userEditForm.patchValue({
          id : response.id,
          username: response.username,
          nom: response.nom,
          prenom: response.prenom,
          cin: response.cin,
          cne: response.cne,
          email: response.email
        });
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        //addForm.reset();
      }
    ); // gtyggt789789
  }

  openRemove(id: any): void { 
    console.log(id);
    //this.displayStyle = "none";
    this.elementToRemove = id;
  }

  onRemove() : void {
    this.userService.deleteUser(this.elementToRemove).subscribe(
      (response) => {
        console.log(response);
        this.users$ = this.userService.getUsers();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        //addForm.reset();
      }
    );
  }

}
