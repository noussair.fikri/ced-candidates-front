import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardModifComponent } from './board-modif.component';

describe('BoardModifComponent', () => {
  let component: BoardModifComponent;
  let fixture: ComponentFixture<BoardModifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardModifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardModifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
