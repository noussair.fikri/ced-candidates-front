import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDocuComponent } from './board-docu.component';

describe('BoardDocuComponent', () => {
  let component: BoardDocuComponent;
  let fixture: ComponentFixture<BoardDocuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDocuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDocuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
