import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardDtheseComponent } from './board-dthese/board-dthese.component';
import { BoardDoctorantComponent } from './board-doctorant/board-doctorant.component';
import { AuthGuard } from './_services/auth.guard';
import { BoardInfogesdoComponent } from './board-infogesdo/board-infogesdo.component';
import { BoardCompteComponent } from './board-compte/board-compte.component';
import { BoardModifComponent } from './board-modif/board-modif.component';
import { BoardInscComponent } from './board-insc/board-insc.component';
import { BoardPreinscComponent } from './board-preinsc/board-preisnc.component';
import { BoardReinscComponent } from './board-reinsc/board-reinsc.component';
import { BoardDocuComponent } from './board-docu/board-docu.component';
import { BoardScoComponent } from './board-sco/board-sco.component';
import { BoardMesdocComponent } from './board-mesdoc/board-mesdoc.component';
import { BoardReclComponent } from './board-recl/board-recl.component';
import { BoardDeporecComponent } from './board-deporec/board-deporec.component';
import { BoardDmdsouComponent } from './board-dmdsou/board-dmdsou.component';
import { BoardSuivrecComponent } from './board-suivrec/board-suivrec.component';
import { BoardCredmComponent } from './board-credm/board-credm.component';
import { BoardSuivsoComponent } from './board-suivso/board-suivso.component';
import { BoardActsciComponent } from './board-actsci/board-actsci.component';
import { BoardMesinfoComponent } from './board-mesinfo/board-mesinfo.component';
import { BoardGessouComponent } from './board-gessou/board-gessou.component';
import { BoardExrapComponent } from './board-exrap/board-exrap.component';



const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  
  // Composants ADMIN
  { path: 'admin', component: BoardAdminComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_ADMIN"} },
  { path: 'dthese', component: BoardDtheseComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DTHESE"} },
  { path: 'doctorant', component: BoardDoctorantComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  //-----ROLE DTHESE------//
  { path: 'infogesdo', component: BoardInfogesdoComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DTHESE"} },
  { path: 'gessou', component: BoardGessouComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DTHESE"} },
  { path: 'exrap', component: BoardExrapComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DTHESE"} },
  //--------ROLE DOCTORANT-----//
  
  { path: 'compte', component: BoardCompteComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'modif', component: BoardModifComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'insc', component: BoardInscComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'preinsc', component: BoardPreinscComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'reinsc', component: BoardReinscComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'docu', component: BoardDocuComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'sco', component: BoardScoComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'recl', component: BoardReclComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'mesdoc', component: BoardMesdocComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'deporec', component: BoardDeporecComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'suivrec', component: BoardSuivrecComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'dmdsou', component: BoardDmdsouComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'credm', component: BoardCredmComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'suivso', component: BoardSuivsoComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'mesinfo', component: BoardMesinfoComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: 'actsci', component: BoardActsciComponent, canActivate: [AuthGuard], data: { expectedRole: "ROLE_DOCTORANT"} },
  { path: '', redirectTo: 'home', pathMatch: 'full' }

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
