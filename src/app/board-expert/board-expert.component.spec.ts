import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDlaboComponent } from './board-dlabo.component';

describe('BoardDlaboComponent', () => {
  let component: BoardDlaboComponent;
  let fixture: ComponentFixture<BoardDlaboComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDlaboComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDlaboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
