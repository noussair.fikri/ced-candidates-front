import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardActsciComponent } from './board-actsci.component';

describe('BoardActsciComponent', () => {
  let component: BoardActsciComponent;
  let fixture: ComponentFixture<BoardActsciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardActsciComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardActsciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
