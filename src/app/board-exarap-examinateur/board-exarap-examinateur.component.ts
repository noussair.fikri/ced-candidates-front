import { Component, OnInit, PipeTransform, QueryList, ViewChildren } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { UserService } from '../_services/user.service';
import { DecimalPipe } from '@angular/common';
import { NgbdSortableHeader, SortEvent } from '../_directives/sortable.directive';
import { User } from '../_models/user';
import { Observable } from 'rxjs';
import { UserTableService } from '../_services/user.table.service';
import { FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-board-exarap-examinateur',
  templateUrl: './board-exarap-examinateur.component.html',
  styleUrls: ['./board-exarap-examinateur.component.css'],
  providers: [UserTableService, DecimalPipe]
})



export class BoardExaRapExaminateurComponent implements OnInit {
  content?: string;
  isLoggedIn = false;

  users$!: Observable<User[]>;
  total$!: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
 
  constructor(public userTableService: UserTableService, private userService: UserService, private tokenStorageService: TokenStorageService, private formBuilder: FormBuilder,private router:Router) { 
    this.users$ = userService.getUsers();
    this.total$ = userTableService.total$;
  }

  userForm = this.formBuilder.group({
    username: '',
    nom: '',
    prenom: '',
    cin: '',
    cne: '',
    email: '' 
  });

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.userTableService.sortColumn = column;
    this.userTableService.sortDirection = direction;
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

  onSubmit(): void {
    this.userService.addUser(this.userForm.value).subscribe(
      (response: User) => {
        console.log(response);
        this.users$ = this.userService.getUsers();
        this.userForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        //addForm.reset();
      }
    );

  }

  generateUsername(): void {
    if(this.userForm.value['nom'] != null && this.userForm.value['prenom'] != null)
      this.userForm.controls['username'].setValue(this.userForm.value['prenom']+'.'+this.userForm.value['nom']);
  }
  goToRegister(){
    this.router.navigate(["/user"])
    }
  
}

