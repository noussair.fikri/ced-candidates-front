import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardExaRapExaminateurComponent } from './board-exarap-examinateur.component';

describe('BoardExaRapExaminateurComponent', () => {
  let component:BoardExaRapExaminateurComponent;
  let fixture: ComponentFixture<BoardExaRapExaminateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardExaRapExaminateurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardExaRapExaminateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
