import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardInfogesdoComponent } from './board-infogesdo.component';

describe('BoardInfogesdoComponent', () => {
  let component: BoardInfogesdoComponent;
  let fixture: ComponentFixture<BoardInfogesdoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardInfogesdoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardInfogesdoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
