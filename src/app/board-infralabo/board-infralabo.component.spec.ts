import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardInfraLaboComponent } from './board-infralabo.component';

describe('BoardInfraLaboComponent', () => {
  let component: BoardInfraLaboComponent;
  let fixture: ComponentFixture<BoardInfraLaboComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardInfraLaboComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardInfraLaboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
