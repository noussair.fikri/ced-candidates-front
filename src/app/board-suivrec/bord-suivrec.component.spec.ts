import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardSuivrecComponent } from './board-suivrec.component';

describe('BoardSuivrecComponent', () => {
  let component: BoardSuivrecComponent;
  let fixture: ComponentFixture<BoardSuivrecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardSuivrecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardSuivrecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
