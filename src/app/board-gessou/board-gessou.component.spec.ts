import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardGessouComponent } from './board-gessou.component';

describe('BoardGessouComponent', () => {
  let component: BoardGessouComponent;
  let fixture: ComponentFixture<BoardGessouComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardGessouComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardGessouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
