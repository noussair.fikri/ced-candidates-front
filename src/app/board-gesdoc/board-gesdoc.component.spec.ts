import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardGesDocComponent } from './board-gesdoc.component';

describe('BoardGesDocComponent', () => {
  let component: BoardGesDocComponent;
  let fixture: ComponentFixture<BoardGesDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardGesDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardGesDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
