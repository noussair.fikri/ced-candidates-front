import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardSoutComponent } from './board-sout.component';

describe('BoardSoutComponent', () => {
  let component: BoardSoutComponent;
  let fixture: ComponentFixture<BoardSoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardSoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardSoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
