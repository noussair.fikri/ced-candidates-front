import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardDeroComponent } from './board-dero.component';

describe('BoardDeroComponent', () => {
  let component: BoardDeroComponent;
  let fixture: ComponentFixture<BoardDeroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDeroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
