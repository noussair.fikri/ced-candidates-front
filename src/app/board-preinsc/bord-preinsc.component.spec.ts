import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardPreinscComponent } from './board-preisnc.component';

describe('BoardPreinscComponent', () => {
  let component: BoardPreinscComponent;
  let fixture: ComponentFixture<BoardPreinscComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardPreinscComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardPreinscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
