import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardCompteComponent } from './board-compte.component';

describe('BoardCompteComponent', () => {
  let component: BoardCompteComponent;
  let fixture: ComponentFixture<BoardCompteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardCompteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
