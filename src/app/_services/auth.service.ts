import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { User } from "../_models/user";
import { TokenStorageService } from "./token-storage.service";
import { UserService } from "./user.service";

@Injectable()
export class AuthService {

  public clientId = 'authserver';
  public clientSecret = 'passwordforauthserver';
  public redirectUri = 'http://localhost:4200';

  constructor(private _http: HttpClient, private tokenStorage: TokenStorageService, private userService : UserService) { }

  retrieveToken(code: string) {
    let params = new URLSearchParams();   
    params.append('grant_type','authorization_code');
    // params.append('client_id', 'authserver');
    // params.append('client_secret', 'passwordforauthserver');
    params.append('redirect_uri', this.redirectUri);
    params.append('code',code);
    console.log(code);

    let headers = 
      new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
                                        'Access-Control-Allow-Origin': '*',
                                        'Authorization' : 'Basic ' + btoa( this.clientId + ':' + this.clientSecret ) });
       
      this._http.post('http://localhost:7070/auth/oauth/token', 
        params.toString(), { headers: headers })
        .subscribe(
          data => this.saveToken(data),
          err => alert('Invalid Credentials')); 
  }

  revokeToken() {

    this.userService.revokeUser().subscribe(
      data => {
        console.log(data);
        //this.tokenStorage.saveUser(data);
      },
      err => {
        console.log(JSON.parse(err.error).message);
      }
    );

    // const httpOptions = {
    //   headers:       new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
    //   'Access-Control-Allow-Origin': '*',
    //   'Authorization' : 'Bearer ' + this.tokenStorage.getToken() }), body: ''
    // };
  
    // let headers = 
    //   new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
    //                     'Access-Control-Allow-Origin': '*',
    //                     'Authorization' : 'Bearer ' + this.tokenStorage.getToken() });
    //   console.log(httpOptions);
    //   this._http.post('http://localhost:7070/auth/refresh/revoke', {}, { headers: headers } ).subscribe(
    //     data => {
    //       console.log("Deleted");
    //     },
    //     err => {
    //       console.log(err.error);
    //     }
    //   );
  }

  retrieveUser() {
    this.userService.getCurrentUser().subscribe(
      data => {
        //console.log(data);
        this.tokenStorage.saveUser(data);
      },
      err => {
        console.log(JSON.parse(err.error).message);
      }
    );
  }

  saveToken(token: any) {
    this.tokenStorage.saveToken(token.access_token);
    this.retrieveUser();
    console.log('Obtained Access token');
  }

  saveUser(user: any) {
    this.tokenStorage.saveUser(user);
    console.log('Obtained User profile');
  }

  getResource(resourceUrl : string) : Observable<any> {
    var headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
      'Authorization': 'Bearer '+this.tokenStorage.getToken()});
    return this._http.get(resourceUrl, { headers: headers });
  }

  checkCredentials() {
    return this.tokenStorage.getToken();
  } 

}