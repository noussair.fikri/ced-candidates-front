import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';

@Injectable({
    providedIn: 'root',
  })
  
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

    private roles: any[] = [];
    isLoggedIn = false;
      
    constructor(protected router: Router, private tokenStorageService : TokenStorageService) {}

    canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        throw new Error('Method not implemented.');
    }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        return this.isAuthenticated(route);
    }
  
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        return this.isAuthenticated(childRoute);
    }
    
    protected isAuthenticated(route?: ActivatedRouteSnapshot ) {

        this.isLoggedIn = !!this.tokenStorageService.getToken();

        if (this.isLoggedIn) {

            const user = this.tokenStorageService.getUser();
            this.roles = user.authorities;

            // this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
            // this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

            // this.username = user.username;
            
        }

        let allowedUserType = true;
        let loggedInUserType = "dark-side"
        let expectedRole: string  
        if (route) {
        expectedRole = route.data.expectedRole;    
            if (expectedRole) { // .some(food => food === '🍰')
                console.log(this.roles);
                allowedUserType = this.roles.some(e => e.authority.includes(expectedRole));
            }
        }
    
        if (!allowedUserType) {
            return false;
        }
    
        return true;
    }

}