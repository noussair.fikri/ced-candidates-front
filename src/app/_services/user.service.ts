import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

const API_URL = 'http://localhost:9000/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }

  addUser(user: User): Observable<User> { // http://localhost:9000/user/add
    return this.http.post<User>(`${API_URL}/add`, user);
  }

  updUser(user: User): Observable<User> {
    return this.http.put<User>(`${API_URL}/update`, user);
  }

  deleteUser(id: any) : Observable<User> {
    return this.http.delete<User>(`${API_URL}/delete/`+id);
  }

  getUser(id: any): Observable<User> {
    return this.http.get<User>(`${API_URL}/find/`+id);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${API_URL}/all`);
  }

  getCurrentUser(): Observable<any> {
    return this.http.get<any>(`${API_URL}/currentUser`);
  }

  revokeUser(): Observable<any> {
    return this.http.get<any>(`${API_URL}/logout`);
  }

}
