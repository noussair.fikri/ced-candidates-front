import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardAdminRolesComponent } from './board-admin-roles.component';

describe('BoardAdminComponent', () => {
  let component: BoardAdminRolesComponent;
  let fixture: ComponentFixture<BoardAdminRolesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardAdminRolesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardAdminRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
