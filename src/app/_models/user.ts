export interface User {
    id: number;
    username: string;
    nom: string;
    prenom: string;
    cin: string;
    cne: string;
    email: string;
  }