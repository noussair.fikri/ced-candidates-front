import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDtheseComponent } from './board-dthese.component';

describe('BoardDtheseComponent', () => {
  let component: BoardDtheseComponent;
  let fixture: ComponentFixture<BoardDtheseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDtheseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDtheseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
