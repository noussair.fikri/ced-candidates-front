import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardReinscComponent } from './board-reinsc.component';

describe('BoardReinscComponent', () => {
  let component: BoardReinscComponent;
  let fixture: ComponentFixture<BoardReinscComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardReinscComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardReinscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
