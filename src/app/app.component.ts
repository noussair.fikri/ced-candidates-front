import { Component } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { TokenStorageService } from './_services/token-storage.service';
import { UserService } from './_services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private roles: any[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showDtheseBoard = false;
  showDoctorantBoard = false;
  showInfogesdoBoard = false;
  showCompteBoard = false;
  showModifBoard = false;
  showInscBoard = false;
  showPreinsc = false;
  showReinsc = false;
  showDocu = false;
  showSco = false;
  showMesdoc = false;
  showRecl = false;
  showDeporec = false;
  showSuivrec = false;
  showDmdsou = false;
  showCredm = false;
  showSuivso = false;
  showActsci = false;
  showMesinfo = false;
  showGessou = false;
  showExrap = false;

  username?: string;
  title: any;

  constructor(private _service: AuthService, private tokenStorageService: TokenStorageService, private userService: UserService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {

      const user = this.tokenStorageService.getUser();
      this.roles = user.authorities;
      console.log(this.tokenStorageService.getUser());
      this.username = user.username;

    }

    this.isLoggedIn = !!this._service.checkCredentials();    
    let i = window.location.href.indexOf('code');
    if(!this.isLoggedIn && i != -1) {
      this._service.retrieveToken(window.location.href.substring(i + 5));
     // this._service.retrieveUser();
    }

  }

  login() {
      window.location.href = 
        'http://localhost:7070/auth/oauth/authorize?response_type=code&client_id=authserver&redirect_uri=http://localhost:4200';
    }

  logout(): void {
    //this._service.revokeToken();
    this.tokenStorageService.signOut();
    // window.location.href = 'http://localhost:7070/auth/oauth/authorize?response_type=code&client_id=authserver&redirect_uri=http://localhost:4200';
  }
}
