import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardCredmComponent } from './board-credm.component';

describe('BoardCredmComponent', () => {
  let component: BoardCredmComponent;
  let fixture: ComponentFixture<BoardCredmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardCredmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardCredmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
