import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardScoComponent } from './board-sco.component';

describe('BoardScoComponent', () => {
  let component: BoardScoComponent;
  let fixture: ComponentFixture<BoardScoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardScoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardScoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
