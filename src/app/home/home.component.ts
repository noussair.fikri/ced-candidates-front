import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { UserService } from '../_services/user.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private roles: any[] = [];

  content?: string;
  isLoggedIn = false;

  showAdminBoard = false;
  showDtheseBoard =false;
  showDoctorantBoard =false;
  showDlaboBoard = false;
  showVicedoyBoard = false;
  
  
  

  username?: string;
  title: any;

  constructor(private userService: UserService, private tokenStorageService: TokenStorageService ,private router :Router) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      //console.log(user);
      this.roles = user.authorities;

      this.showAdminBoard = this.roles.some(e => e.authority.includes('ROLE_ADMIN'));
      this.showDtheseBoard = this.roles.some(e => e.authority.includes('ROLE_DTHESE'));
      this.showDoctorantBoard = this.roles.some(e => e.authority.includes('ROLE_DOCTORANT'));
      
      this.username = user.username;

    }
  }
  goToRegiste(){
    this.router.navigate(["/user2"])
    }


}
