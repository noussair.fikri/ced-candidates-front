import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDmdsouComponent } from './board-dmdsou.component';

describe('BoardDmdsouComponent', () => {
  let component: BoardDmdsouComponent;
  let fixture: ComponentFixture<BoardDmdsouComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDmdsouComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDmdsouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
