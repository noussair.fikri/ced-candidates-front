import {User} from '../_models/user';

export const USERS: User[] = [
    {     
        id: 1,
        username: 'noussair.fikri',
        nom: 'Fikri',
        prenom: 'Noussair',
        cin: 'BKxxxxxx',
        cne: 'xxxxxxxx',
        email: 'noussair.fikri@gmail.com' 
    }
]