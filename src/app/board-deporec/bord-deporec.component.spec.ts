import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDeporecComponent } from './board-deporec.component';

describe('BoardDeporecComponent', () => {
  let component: BoardDeporecComponent;
  let fixture: ComponentFixture<BoardDeporecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardDeporecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDeporecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
