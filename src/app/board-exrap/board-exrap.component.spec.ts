import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardExrapComponent } from './board-exrap.component';

describe('BoardExrapComponent', () => {
  let component: BoardExrapComponent;
  let fixture: ComponentFixture<BoardExrapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardExrapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardExrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
