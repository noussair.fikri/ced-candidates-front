import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardSuivsoComponent } from './board-suivso.component';

describe('BoardSuivsoComponent', () => {
  let component: BoardSuivsoComponent;
  let fixture: ComponentFixture<BoardSuivsoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardSuivsoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardSuivsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
