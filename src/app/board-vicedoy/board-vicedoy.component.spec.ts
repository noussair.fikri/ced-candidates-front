import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardVicedoyComponent } from './board-vicedoy.component';

describe('BoardVicedoyComponent', () => {
  let component: BoardVicedoyComponent;
  let fixture: ComponentFixture<BoardVicedoyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardVicedoyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardVicedoyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
